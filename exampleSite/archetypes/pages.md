---
title:  "{{ replace .Name "-" " " | title }}"
date:  {{ .Date }}
draft: true
tags: []
slug: "{{ lower .Name }}"
id: "{{ .File.UniqueID }}"
---
