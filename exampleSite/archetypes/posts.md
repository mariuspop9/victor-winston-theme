---
title:  "{{ replace .Name "-" " " | title }}"
date:  {{ .Date }}
draft: true
tags: []
slug: "{{ lower .Name }}"
id: "{{ .File.UniqueID }}"
description: 'As soon as Winston had dealt with each of the messages, he clipped his speakwritten corrections to the appropriate copy of the Times and pushed them into the pneumatic tube. '
---